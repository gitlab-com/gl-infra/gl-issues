package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"

	"gitlab.com/gl-infra/gl-issues/config"
	"gitlab.com/gl-infra/gl-issues/issue"
)

const settingsFname = ".gl-issues.yaml"

func main() {
	var filepath string

	if _, err := os.Stat(path.Join(".", settingsFname)); err == nil {
		filepath = path.Join(".", settingsFname)
	}

	if _, err := os.Stat(path.Join(os.Getenv("HOME"), settingsFname)); err == nil {
		filepath = path.Join(os.Getenv("HOME"), settingsFname)
	}

	if _, err := os.Stat(filepath); os.IsNotExist(err) {
		log.Fatal(fmt.Sprintf("ERROR: Unable to locate %s", settingsFname))
	}

	cfgFile := flag.String("config", filepath, "the configuration file")
	user := flag.String("user", "", "user for report (default to users in config)")
	markdown := flag.Bool("markdown", false, "display in markdown")
	html := flag.Bool("html", false, "display in html")

	flag.Parse()

	c, err := config.ReadConfig(*cfgFile)
	if err != nil {
		log.Fatalln(err)
	}
	opts := issue.Options{
		GitLabToken: c.Token,
		Markdown:    *markdown,
		HTML:        *html,
		EnableUTF8:  true,
	}
	if *user != "" {
		c.Users = []string{*user}
	}
	for _, u := range c.Users {
		report := issue.NewReport(opts)
		r, err := report.RenderUserReport(u)
		if err != nil {
			log.Fatalf("Report for user %s failed to render: %s", u, err)
		}
		fmt.Println(r)
	}
}
