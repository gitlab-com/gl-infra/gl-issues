package config

import (
	"reflect"
	"testing"
)

func TestReadConfig(t *testing.T) {
	filename := "invalid-cfg.json"
	_, err := ReadConfig(filename)
	if err == nil {
		t.Fatalf("could not read configuration file %s", filename)
	}

	want := "could not stat file invalid-cfg.json: stat invalid-cfg.json: no such file or directory"
	got := err.Error()
	if got != want {
		t.Fatalf("wrong configuration error, got: %s; expected %s", got, want)
	}
}

func assertEquals(t *testing.T, name string, expected, actual interface{}) {
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("%s is not as expected: %+v; got %+v", name, expected, actual)
	}
}
