package config

import (
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

// Config for top level yaml settings
type Config struct {
	Token string   `yaml:"token"`
	Users []string `yaml:"users"`
}

// ReadConfig will attempt to read the different configuration
// parameters from a yaml formatted file.
func ReadConfig(f string) (*Config, error) {
	var cfg Config
	if _, err := os.Stat(f); os.IsNotExist(err) {
		return nil, fmt.Errorf("could not stat file %s: %s", f, err)
	}
	content, err := ioutil.ReadFile(f)
	if err != nil {
		return nil, fmt.Errorf("could not read file %s: %s", f, err)
	}
	err = yaml.Unmarshal(content, &cfg)
	if err != nil {
		return nil, fmt.Errorf("could unmarshal content %s: %s", content, err)
	}
	return &cfg, nil
}
