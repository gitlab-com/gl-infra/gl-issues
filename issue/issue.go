package issue

import (
	"fmt"
	"strings"

	"github.com/apcera/termtables"
	gitlab "github.com/xanzy/go-gitlab"
)

// Options has options for the issue report
type Options struct {
	Client      ReportClient
	GitLabToken string
	Markdown    bool
	HTML        bool
	ASCII       bool
	EnableUTF8  bool
}

// Report is the issue report
type Report struct {
	client     ReportClient
	html       bool
	markdown   bool
	enableUTF8 bool
	projCache  map[int]string
	userCache  map[string]int
}

// NewReport initializes the issue report
func NewReport(opts Options) *Report {
	r := Report{
		client:     newGitlabClient(opts.Client, opts.GitLabToken),
		markdown:   opts.Markdown,
		enableUTF8: opts.EnableUTF8,
		html:       opts.HTML,
		projCache:  make(map[int]string),
		userCache:  make(map[string]int),
	}
	return &r
}

// ReportClient is the interface for the client that talks to gitlab
type ReportClient interface {
	listIssues(opts *gitlab.ListIssuesOptions) ([]*gitlab.Issue, *gitlab.Response, error)
	listUsers(opts *gitlab.ListUsersOptions) ([]*gitlab.User, *gitlab.Response, error)
	getProject(pid interface{}) (*gitlab.Project, *gitlab.Response, error)
}

// gitLabClient wraps gitlab api operations for listing users,
// listing options and getting projects
// and implements ReportClient
type gitLabClient struct {
	client *gitlab.Client
}

func (g *gitLabClient) listIssues(opts *gitlab.ListIssuesOptions) ([]*gitlab.Issue, *gitlab.Response, error) {
	return g.client.Issues.ListIssues(opts)
}
func (g *gitLabClient) listUsers(opts *gitlab.ListUsersOptions) ([]*gitlab.User, *gitlab.Response, error) {
	return g.client.Users.ListUsers(opts)
}
func (g *gitLabClient) getProject(pid interface{}) (*gitlab.Project, *gitlab.Response, error) {
	return g.client.Projects.GetProject(pid)
}

func newGitlabClient(c ReportClient, token string) ReportClient {
	if c != nil {
		return c
	}
	newClient := gitLabClient{
		client: gitlab.NewClient(nil, token),
	}
	return &newClient
}

// RenderUserReport - creates an issue report for a gitlab users
func (r *Report) RenderUserReport(user string) (string, error) {
	uid, err := r.getUIDForUsername(user)
	if err != nil {
		return "", err
	}
	var state = "opened"
	var scope = "all"
	opts := gitlab.ListIssuesOptions{
		State:      &state,
		Scope:      &scope,
		AssigneeID: &uid,
	}
	issues, _, err := r.client.listIssues(&opts)
	if err != nil {
		return "", err
	}
	if r.enableUTF8 {
		termtables.EnableUTF8PerLocale()
	}
	termtables.SetModeHTML(r.html)
	termtables.SetModeMarkdown(r.markdown)
	t := termtables.CreateTable()
	t.AddHeaders("Project", "Title", "Link")
	for _, issue := range issues {
		pname, err := r.getProjectNameFromID(issue.ProjectID)
		if err != nil {
			return "", fmt.Errorf("Unable to find project name for pid %d: %s", issue.ProjectID, err)
		}
		t.AddRow(pname, issue.Title, strings.Replace(issue.WebURL, "https://", "", 1))
	}
	t.AddTitle(user)
	return t.Render(), nil
}

func (r *Report) getUIDForUsername(user string) (int, error) {
	v, ok := r.userCache[user]
	if ok {
		return v, nil
	}
	userOpts := gitlab.ListUsersOptions{}
	userOpts.Username = &user
	users, _, err := r.client.listUsers(&userOpts)
	if err != nil {
		return 0, err
	}
	if len(users) != 1 {
		return 0, fmt.Errorf("User %s not found", user)
	}
	return users[0].ID, nil
}

func (r *Report) getProjectNameFromID(pid int) (string, error) {
	v, ok := r.projCache[pid]
	if ok {
		return v, nil
	}
	p, _, err := r.client.getProject(pid)
	if err != nil {
		return "", err
	}
	r.projCache[pid] = p.Name
	return p.Name, nil
}
