package issue

import (
	"io/ioutil"
	"log"
	"os"
	"testing"

	gitlab "github.com/xanzy/go-gitlab"
)

type StubClient struct {
}

func (s *StubClient) listIssues(opts *gitlab.ListIssuesOptions) ([]*gitlab.Issue, *gitlab.Response, error) {
	issues := make([]*gitlab.Issue, 2)
	for i := range issues {
		issues[i] = &gitlab.Issue{
			ProjectID: 9000,
			Title:     "make everything great",
			WebURL:    "http://example.com",
		}
	}

	resp := gitlab.Response{}
	return issues, &resp, nil
}

func (s *StubClient) listUsers(opts *gitlab.ListUsersOptions) ([]*gitlab.User, *gitlab.Response, error) {
	users := make([]*gitlab.User, 1)
	for i := range users {
		users[i] = new(gitlab.User)
	}
	resp := gitlab.Response{}
	return users, &resp, nil
}

func (s *StubClient) getProject(pid interface{}) (*gitlab.Project, *gitlab.Response, error) {
	proj := gitlab.Project{
		Name: "Amazing project!!!11",
		ID:   9000,
	}
	resp := gitlab.Response{}
	return &proj, &resp, nil
}

func TestMain(m *testing.M) {
	// Turn off logging for the tests
	log.SetOutput(ioutil.Discard)
	retCode := m.Run()
	os.Exit(retCode)
}

func TestRenderUserReportPlain(t *testing.T) {
	sc := StubClient{}
	opts := Options{
		GitLabToken: "abcd123",
		Client:      &sc,
	}
	report := NewReport(opts)
	r, err := report.RenderUserReport("herpderp")
	if err != nil {
		t.Fatalf("Report render failed: %s", err.Error())
	}
	expectedReport := `+-------------------------------------------------------------------+
|                             herpderp                              |
+----------------------+-----------------------+--------------------+
| Project              | Title                 | Link               |
+----------------------+-----------------------+--------------------+
| Amazing project!!!11 | make everything great | http://example.com |
| Amazing project!!!11 | make everything great | http://example.com |
+----------------------+-----------------------+--------------------+
`
	if r != expectedReport {
		t.Fatalf("\nExpected report:\n%s\nDoesn't match generated report:\n%s", expectedReport, r)
	}
}
