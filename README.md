# Team issues

This is a command line wrapper for querying issue
stats on gitlab.com

# How to use

* `go get -d gitlab.com/gl-infra/gl-issues`
* Install dep https://github.com/golang/dep#setup
* `dep init`
* `go build`
* `go install`
* Copy `gl-issues-example.yaml` to `~/.gl-issues.yaml` and update team members (gl usernames) and your gitlab token.
* Run `gl-issues` to output issue stats or `gl-issues -user <name>` to issue a report for a single user
